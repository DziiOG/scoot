import {createStackNavigator} from 'react-navigation-stack';
import Help from '../routes/Help/components/Help';
const screens = {
  Help: {
    screen: Help,
  },
};

const HelpStack = createStackNavigator(screens);
export default HelpStack;
