import {createStackNavigator} from 'react-navigation-stack';
import Trips from '../routes/Trips/components/Trips';
const screens = {
  Trips: {
    screen: Trips,
  },
};

const TripStack = createStackNavigator(screens);
export default TripStack;
