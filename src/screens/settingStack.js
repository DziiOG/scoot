import {createStackNavigator} from 'react-navigation-stack';
import Settings from '../routes/Settings/components/Settings';
const screens = {
  Settings: {
    screen: Settings,
  },
};

const SettingStack = createStackNavigator(screens);
export default SettingStack;
