import {createStackNavigator} from 'react-navigation-stack';
import FreeRides from '../routes/FreeRides/components/FreeRides';
const screens = {
  FreeRides: {
    screen: FreeRides,
  },
};

const FreeRideStack = createStackNavigator(screens);
export default FreeRideStack;
