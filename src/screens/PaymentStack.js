import {createStackNavigator} from 'react-navigation-stack';
import Payments from '../routes/Payment/components/Payment';
const screens = {
  Payments: {
    screen: Payments,
  },
};

const PaymentStack = createStackNavigator(screens);
export default PaymentStack;
