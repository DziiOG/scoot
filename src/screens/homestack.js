import {createStackNavigator} from 'react-navigation-stack';

import Home from '../routes/Home/components/Home';

const screens = {
  Home: {
    screen: Home,
  },
};

const HomeStack = createStackNavigator(screens);
export default HomeStack;
