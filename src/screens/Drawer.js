import {createDrawerNavigator} from 'react-navigation-drawer';

import {createAppContainer} from 'react-navigation';
import TripStack from './TripStack';
import FreeRideStack from './freeRideStack';
import HelpStack from './HelpStack';
import PaymentStack from './PaymentStack';
import SettingStack from './settingStack';
import HomeStack from './homestack';
const RootDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeStack,
  },
  Trips: {
    screen: TripStack,
  },
  Payment: {
    screen: PaymentStack,
  },
  Help: {
    screen: HelpStack,
  },
  FreeRides: {
    screen: FreeRideStack,
  },
  Settings: {
    screen: SettingStack,
  },
});

const Navigator = createAppContainer(RootDrawerNavigator);
export default Navigator;
