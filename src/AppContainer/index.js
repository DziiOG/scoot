import React, {Component} from 'react';
import {Router} from 'react-native-router-flux';
import scenes from '../routes/scenes.js';
import PropTypes from 'prop-types';
import {Provider} from 'react-redux';
export default class AppContainer extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
         <Router scenes={scenes}></Router> 
      </Provider>
    );
  }
}

AppContainer.propTypes = {
  store: PropTypes.object.isRequired,
};
