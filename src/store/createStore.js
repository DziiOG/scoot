import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import makeRootReducer from './reducers';
import {createLogger} from 'redux-logger';

const log = createLogger({
  diff: true,
  collapsed: true,
});

//a function for auto persisting of data and can create the store
export default (initialState = {}) => {
  const middleware = [thunk, log];

  //store enhancers
  const enhancers = [];

  //Store intantiation
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(applyMiddleware(...middleware), ...enhancers),
  );
  return store;
};
