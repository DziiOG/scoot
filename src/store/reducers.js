import { combineReducers } from "redux";
//combine reducers makes all reducers into one function
//combines all functions from different routes and then turns it into  a single reducing function
import { HomeReducer as home } from "../routes/Home/modules/home";
import { freeRidesReducer as freeRides} from "../routes/FreeRides/modules/freeRides";

import { tripsReducer as trips} from "../routes/Trips/modules/trips";
import { settingsReducer as settings} from "../routes/Settings/modules/settings";
import { helpReducer as help} from "../routes/Help/modules/help";
import { paymentReducer as payment} from "../routes/Payment/modules/payment";
import { loginReducer as login} from "../routes/Login/modules/login";
import { registerPageReducer as registerPage} from "../routes/SignUp/modules/registerPage";



export const makeRootReducer = () => {
    return combineReducers({
        home,
        freeRides,
        trips,
        help,
        settings,
        payment,
        login,
        registerPage,
        
    })
}

export default makeRootReducer;     