import {connect} from 'react-redux';
import RegisterPage from '../components/RegisterPage';
import {saveUserToken, getUserID, getPicUrl} from '../modules/registerPage';

const mapStateToProps = (state) => ({
  userID: state.registerPage.userID || {},
  imageUrl: state.registerPage.imageUrl || {},
});

const mapActionsCreators = {
  saveUserToken,
  getUserID,
  getPicUrl,
};

export default connect(mapStateToProps, mapActionsCreators)(RegisterPage);
