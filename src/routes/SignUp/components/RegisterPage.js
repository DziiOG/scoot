import React, {Component, Fragment} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ImageBackground,
  TextInput,
  Modal,
  NativeMethodsMixin,
  ActivityIndicator,
} from 'react-native';
import {Container, Content, Input, Header, Item} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import * as Animatable from 'react-native-animatable';
import {Actions} from 'react-native-router-flux';

export default class RegistrationPage extends Component {
  state = {
    username: '',
    email: '',
    password: '',
    errors: {},
    modalAppear: false,
    loading: false,
    submitButtonBool: true,
    token: '',
  };
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (text, type) => {
    if (type == 'username') {
      this.setState({
        username: text,
      });
    }
    if (type == 'email') {
      this.setState({
        email: text,
      });
    }
    if (type == 'password') {
      this.setState({
        password: text,
      });
    }
  };

  handleCreateAccount = () => {
    this.setState({
      modalAppear: true,
    });
  };

  handleModalOff = () => {
    this.setState({
      modalAppear: false,
    });
  };
  handleSubmit = async () => {
    this.setState({
      loading: true,
      submitButtonBool: false,
    });
    axios
      .post('https://us-central1-halo-84fb8.cloudfunctions.net/api/signup', {
        email: this.state.email,
        password: this.state.password,
        username: this.state.username,
      })
      .then((results) => {
        console.log(results.data.token);
        console.log(results.data.imageUrl);
        this.setState({
          modalAppear: false,
          token: results.data.token,
          // this state keeps the url of the userprofile so pass this with to the next page redux
          // imageUrl: results.data.imageUrl,
        });
        console.log(results.data.token);
        this.props.getUserID(results.data.userId);
        this.props.getPicUrl(results.data.imageUrl);
        Actions.reset('root');
      })
      .catch((err) => {
        console.log(err.response.data);
        //console.log(err);
        this.setState({
          loading: false,
          submitButtonBool: true,
          errors: err.response.data
        });

        if(this.state.errors.email){
          Alert.alert("Email must not be empty or enter a correct email address")
        }
        if(this.state.errors.password){
          Alert.alert("Password must not be empty ")
        }
        if(this.state.errors.username){
          Alert.alert("Enter correct username or user already exists")
        }
      });
  };

  render() {
    return (
      <Fragment>
        <Modal
          animated
          visible={this.state.modalAppear}
          animationType="slide"
          onRequestClose={() => {
            this.handleModalOff();
          }}>
          <Container>
            {/* <Header
              style={{
                backgroundColor: '#fff',
              }}>
              <Icon
                name="arrow-left"
                size={25}
                color="#5cccee"
                style={{left: -170, marginTop: 15}}
                onPress={() => {
                  this.handleModalOff();
                }}></Icon>
            </Header> */}
            <Animatable.View
              style={{
                backgroundColor: '#5cccee',
                height: '50%',
                width: '100%',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center',
              }}>
              <Animatable.View
                animation="fadeIn"
                iterationDelay={2}
                style={{
                  height: 150,
                  width: 150,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#fff',
                }}>
                <Animatable.Text
                  animation="zoomIn"
                  iterationCount={1}
                  iterationDelay={1}
                  style={{
                    color: '#5cccee',
                    fontSize: 30,
                    textDecorationLine: 'underline',
                  }}>
                  SCOOT
                </Animatable.Text>
              </Animatable.View>
            </Animatable.View>
            <View
              style={{
                flex: 1,
                paddingVertical: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 10,
                  paddingLeft: 50,
                  justifyContent: 'center',
                }}>
                <Icon
                  active
                  name="user"
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  color="#5cccee"
                  style={{paddingTop: 10}}
                  size={30}
                />
                <Input
                  placeholder="Username"
                  color="#5cccee"
                  placeholderTextColor="#5cccee"
                  defaultValue={this.state.username}
                  errorMessage={this.state.errors.username}
                  onChangeText={(text) => this.handleChange(text, 'username')}
                  style={styles.input}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 10,
                  paddingLeft: 50,
                }}>
                <Icon3
                  active
                  name="email"
                  style={{paddingTop: 10}}
                  size={30}
                  color="#5cccee"
                />
                <Input
                  placeholder="Email"
                  color="#5cccee"
                  placeholderTextColor="#5cccee"
                  defaultValue={this.state.email}
                  errorMessage={this.state.errors.email}
                  onChangeText={(text) => this.handleChange(text, 'email')}
                  style={styles.input}
                  keyboardType="email-address"
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 10,
                  paddingLeft: 50,
                }}>
                <Icon2
                  active
                  name="key"
                  style={{paddingTop: 10}}
                  size={30}
                  color="#5cccee"
                />
                <Input
                  placeholder="Password"
                  color="#5cccee"
                  placeholderTextColor="#5cccee"
                  secureTextEntry
                  defaultValue={this.state.password}
                  errorMessage={this.state.errors.password}
                  onChangeText={(text) => this.handleChange(text, 'password')}
                  style={{...styles.input, marginBottom: 10}}
                />
              </View>
              {this.state.submitButtonBool && (
                <View style={styles.button}>
                  <View
                    style={{
                      width: '100%',
                      alignViews: 'center',
                    }}>
                    <TouchableOpacity style={styles.buttonText}>
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'center',
                        }}
                        onPress={this.handleSubmit}>
                        PROCEED
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
              {this.state.loading && (
                <ActivityIndicator size="large" color="#5cccee" />
              )}
            </View>
          </Container>
        </Modal>
        <View style={{flex: 1}}>
          <ImageBackground
            source={require('../../../assets/bence-boros-fbCXIOWFrQQ-unsplash.jpg')}
            style={{flex: 1}}>
            <Animatable.View
              animation="zoomIn"
              iterationCount={1}
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  backgroundColor: '#fff',
                  width: 100,
                  height: 100,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 26,
                    color: '#5cccee',
                    fontWeight: 'bold',
                    textDecorationLine: 'underline',
                  }}>
                  SCOOT
                </Text>
              </View>
            </Animatable.View>

            {/*Bottom half */}
            <Animatable.View animation="slideInUp" iterationCount={1}>
              <View
                style={{
                  height: 150,
                  backgroundColor: '#fff',
                }}>
                <Animatable.View
                  style={{
                    opacity: 1,
                    alignItems: 'flex-start',
                    paddingHorizontal: 25,
                    marginTop: 25,
                  }}>
                  <Text
                    style={{
                      fontSize: 24,
                      color: '#5cccee',
                    }}>
                    Get moving with Scoot
                  </Text>
                </Animatable.View>
                <TouchableOpacity>
                  <View
                    style={{
                      marginTop: 25,
                      paddingHorizontal: 25,
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                      }}>
                      <TouchableOpacity
                        style={{
                          height: 50,
                          borderRadius: 50,
                          backgroundColor: '#5cccee',
                          width: 300,
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginLeft: 25,
                        }}
                        onPress={() => {
                          Actions.Login();
                        }}>
                        <Text
                          style={{
                            color: '#fff',
                            fontSize: 20,
                          }}>
                          Login
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: 70,
                  backgroundColor: '#fff',
                  alignItems: 'flex-start',
                  borderTopColor: '#e8e8ec',
                  justifyContent: 'center',
                  borderWidth: 1,
                  paddingHorizontal: 25,
                }}>
                <Text
                  style={{
                    color: '#5cccee',
                    fontWeight: 'bold',
                  }}
                  onPress={() => {
                    this.handleCreateAccount();
                  }}>
                  Or sign up if you don't have an acoount
                </Text>
              </View>
            </Animatable.View>
          </ImageBackground>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: '100%',
  },
  content: {
    top: 55,
    left: 60,
  },
  placeholder: {
    fontSize: 20,
    opacity: 1,
    color: '#5cccee',
  },
  title: {
    fontSize: 20,
    opacity: 1,
    color: '#5cccee',
    marginTop: 10,
    marginBottom: 10,
    alignContent: 'center',
    justifyContent: 'center',
  },

  input: {
    color: '#5cccee',
    borderRadius: 10,
    textAlign: 'left',
    fontSize: 20,
    marginHorizontal: 40,
  },

  button: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 100,
    marginTop: 20,
  },
  buttonText: {
    borderWidth: 1,
    height: 60,
    width: '70%',
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#5bcced',
    borderRadius: 10,
    borderColor: 'white',
    opacity: 1,
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
