import update from 'react-addons-update';
import constants from './actionConstants';
import AsyncStorage from '@react-native-community/async-storage';

const {SAVE_USER_TOKEN} = constants;

//ACTIONS

export function saveUserToken(token) {
  return (dispatch) => {
    AsyncStorage.setItem('userToken', token).then((data) => {
      dispatch({
        type: SAVE_USER_TOKEN,
        payload,
      });
    });
  };
}

//handlers
const {GET_USER_ID} = constants;
const {GET_PIC_URL} = constants;

//ACTIONS
export function getUserID(userId) {
  return (dispatch) => {
    dispatch({
      type: GET_USER_ID,
      payload: userId,
    });
  };
}

export function getPicUrl(imageUrl) {
  return (dispatch) => {
    dispatch({
      type: GET_PIC_URL,
      payload: imageUrl,
    });
  };
}

//Action handler
function handleGetUserID(state, action) {
  return update(state, {
    userID: {
      $set: action.payload,
    },
  });
}

function handleGetPicUrl(state, action) {
  return update(state, {
    imageUrl: {
      $set: action.payload,
    },
  });
}

const initialState = {
  userID: {},
  imageUrl: {},
};

const ACTION_HANDLERS = {
  GET_USER_ID: handleGetUserID,
  GET_PIC_URL: handleGetPicUrl,
};

export function registerPageReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
