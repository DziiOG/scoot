import 'react-native-gesture-handler';
import React from 'react';
import {Fragment} from 'react-native';
import {Actions, Scene, Drawer} from 'react-native-router-flux';
import HomeContainer from './Home/containers/HomeContainer';
import FreeRidesContainer from './FreeRides/containers/FreeRidesContainer';
import SettingsContainer from './Settings/containers/SettingsContainer';
import TripsContainer from './Trips/containers/TripsContainer';
import PaymentContainer from './Payment/containers/PaymentContainer';
import HelpContainer from './Help/containers/HelpContainer';
import RegisterPageContainer from './SignUp/containers/RegisterPageContainer';
import LoginContainer from './Login/containers/LoginContainer';
import DrawerContent from '../components/Drawer';

//adding this comment to see if my git is working well
const scenes = Actions.create(
  <Fragment>
    <Drawer
      key="root"
      drawerPosition="left"
      headerMode="screen"
      navTransparent={true}
      contentComponent={DrawerContent}>
      <Scene
        key="home"
        component={HomeContainer}
        drawer={true}
        navTransparent
        initial
      />
    </Drawer>
    <Scene key="signup" component={RegisterPageContainer} navTransparent  />
    <Scene key="FreeRides" component={FreeRidesContainer} title="FreeRides" />
    <Scene
      key="Settings"
      component={SettingsContainer}
      title="Settings"
      
    />
    <Scene key="Trips" component={TripsContainer} title="Trips" />
    <Scene key="Payment" component={PaymentContainer} title="Payment" />
    <Scene key="Help" component={HelpContainer} title="Help" />
    <Scene
      key="Login"
      component={LoginContainer}
      title="Login"
      navTransparent
      hideNavBar
    />
  </Fragment>,
);
export default scenes;
