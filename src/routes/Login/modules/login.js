import update from "react-addons-update";
import constants from './actionConstants';

//const {} = constants;

const { 
    GET_USER_ID } = constants;

//ACTIONS
export function getUserID(userId){
    return(dispatch)=>{
        dispatch({
            type: GET_USER_ID,
            payload: userId
        })
    }
}


//Action handler
function handleGetUserID(state, action){
    return update(state, {
        userID: {
            $set: action.payload
        }
    })
}


const initialState = {
    userID: {}
};

const ACTION_HANDLERS = {
    GET_USER_ID : handleGetUserID
 
 }

export function loginReducer (state = initialState, action){
    
    const handler = ACTION_HANDLERS[action.type];

    return handler ? handler(state, action) : state;

}