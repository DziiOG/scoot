import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {Container, Content, Item, Input} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-community/async-storage';

export default class Login extends Component {
  state = {
    email: '',
    password: '',
    errors: {},
    token: '',
    loading: false,
    submitButtonBool: true,
  };
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (text, type) => {
    if (type == 'email') {
      this.setState({
        email: text,
      });
    }
    if (type == 'password') {
      this.setState({
        password: text,
      });
    }
  };

  handleSubmit = async () => {
    this.setState({
      loading: true,
      submitButtonBool: false,
    });

    axios
      .post('https://us-central1-halo-84fb8.cloudfunctions.net/api/login', {
        email: this.state.email,
        password: this.state.password,
      })
      .then(results => {
        console.log(results.data.token);
        this.setState({
          loading: false,
          token: results.data.token,
        });

        this.props.getUserID(results.data.userId)
        Actions.reset('root');
      })
      .catch((err) => {
       
        this.setState({
          loading: false,
          submitButtonBool: true,
          errors: err.response
        });

       
        if(err.message){
          Alert.alert("Make sure fields are not empty and correct credentials have been entered and try again.");
        }
      });
  };

  render() {
    return (
      <View>
        <Animatable.View
          style={{
            backgroundColor: '#5cccee',
            height: '60%',
            width: '100%',
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <Animatable.View
            animation="fadeIn"
            iterationDelay={2}
            style={{
              height: 150,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#fff',
            }}>
            <Animatable.Text
              animation="zoomIn"
              iterationCount={1}
              iterationDelay={1}
              style={{
                color: '#5cccee',
                fontSize: 30,
                textDecorationLine: 'underline',
              }}>
              SCOOT
            </Animatable.Text>
          </Animatable.View>
        </Animatable.View>
        <View style={styles.content}>
          <View
            style={{
              width: '70%',
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <Icon
              active
              name="user"
              style={{paddingTop: 10}}
              size={35}
              color="#5cccee"
            />
            <Input
              keyboardType="email-address"
              placeholder="Email"
              color="#5cccee"
              
              placeholderTextColor="#5cccee"
              defaultValue={this.state.email}
              onChangeText={text => this.handleChange(text, 'email')}
              style={styles.input}></Input>
          </View>

          <View
            style={{
              width: '70%',
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <Icon2 active name="key" size={35} color="#5cccee" />
            <Input
              placeholder="Password"
              color="#5cccee"
              placeholderTextColor="#5cccee"
              secureTextEntry
              
              defaultValue={this.state.password}
              onChangeText={text => this.handleChange(text, 'password')}
              style={styles.input}></Input>
          </View>
          {this.state.submitButtonBool && (
            <View style={styles.button}>
              <View
                style={{
                  width: '100%',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  style={styles.buttonText}
                  onPress={this.handleSubmit}>
                  <Text style={{color: 'white'}}> PROCEED</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
          {this.state.loading && (
            <ActivityIndicator
              size="large"
              color="#5cccee"
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 90,
              }}
            />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: '100%',
  },
  content: {
    top: 55,
    left: 60,
  },
  placeholder: {
    fontSize: 20,
    opacity: 1,
    color: '#5cccee',
  },
  title: {
    fontSize: 20,
    opacity: 1,
    color: '#5cccee',
    marginTop: 10,
    marginBottom: 10,
    alignContent: 'center',
    justifyContent: 'center',
  },

  input: {
    height: 50,
    color: '#5cccee',
    borderRadius: 10,
    width: '50%',

    textAlign: 'center',
    fontSize: 20,
  },

  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
    right: 60,
  },
  buttonText: {
    borderWidth: 1,
    height: 60,
    width: '70%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5bcced',
    borderRadius: 10,
    borderColor: 'white',
    opacity: 1,
  },
});
