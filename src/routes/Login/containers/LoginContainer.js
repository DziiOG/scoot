import { connect } from 'react-redux';

import {getUserID } from "../modules/login";
import Login from '../components/Login';

const mapStateToProps = (state) => ({
    userID: state.login.userID || {}
});

const mapActionsCreators = {
    getUserID
};

export default connect(mapStateToProps, mapActionsCreators)(Login);
