import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  StyleSheet,
  PixelRatio,
  Image,
  Dimensions,
} from 'react-native';
import {Container, Thumbnail, Button} from 'native-base';
import HeaderForSettings from './Header';
import UserIcon from 'react-native-vector-icons/AntDesign';
import MoneyIcon from 'react-native-vector-icons/FontAwesome5';
import PlusIcon from 'react-native-vector-icons/FontAwesome';
import CreditIcon from 'react-native-vector-icons/Entypo';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';

const options = {
  quality: 1.0,
  maxWidth: 500,
  maxHeight: 500,
  noData: true,
  storageOptions: {
    skipBackup: true,
  },
};
class Settings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      imageUrl: 'madman',
      showModalBool: false,
      avatarSource: null,
    };
  }
  componentDidMount() {
    axios
      .post('https://us-central1-halo-84fb8.cloudfunctions.net/api/user', {
        userId: this.props.userID,
      })
      .then(results => {
        console.log(results.data);
        this.setState({
          username: results.data,
        });
      })
      .catch(err => {
        console.log(err);
      });

    axios
      .post('https://us-central1-halo-84fb8.cloudfunctions.net/api/imageUrl', {
        userId: this.props.userID,
      })
      .then(res => {
        this.setState({imageUrl: res.data});
        console.log(this.state.imageUrl);
      })
      .catch(err => {
        console.log(err);
      });
  }

  addPick = () => {
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        console.log(source);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
        });
      }
    });
  };
  render() {
    return (
      <Container>
        {true && (
          <View>
            <View style={{paddingLeft: 30, flexDirection: 'row'}}>
              <View>
                <Thumbnail
                  large
                  source={{
                    // yeah so if you pass the url via redux, it just comes here
                    uri: this.state.imageUrl.toString(),
                  }}
                />
              </View>

              <View style={{paddingTop: 10, paddingHorizontal: 20}}>
                <Text
                  style={{
                    color: '#5cccee',
                    fontSize: 20,
                    textAlign: 'center',
                  }}>
                  {this.state.username}
                </Text>
              </View>
            </View>

            <View
              style={{
                paddingTop: 25,
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 20,
                }}>
                <Button
                  style={{
                    width: '95%',
                    backgroundColor: '#f7f5f5',
                    borderRadius: 15,
                  }}
                  onPress={() => {
                    this.setState({
                      showModalBool: true,
                    });
                  }}>
                  <Text style={{color: '#5cccee', fontSize: 20}}>
                    <UserIcon name="user" size={23} />
                    My Details
                  </Text>
                </Button>
              </View>
              {this.state.showModalBool && (
                <Modal
                  onRequestClose={() => {
                    this.setState({showModalBool: false});
                  }}>
                  <View>
                    <Text>hello nigga</Text>
                    {this.state.avatarSource === null ? (
                      <TouchableOpacity
                        style={styles.avatarPlaceholder}
                        onPress={this.addPick}>
                        <PlusIcon name="plus-square-o" size={40} color="#000" />
                      </TouchableOpacity>
                    ) : (
                        <Image
                           source={this.state.avatarSource}
                           style={styles.uploadPic}
                         />
                      
                    )}
                  </View>
                </Modal>
              )}
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 20,
                }}>
                <Button
                  style={{
                    width: '95%',
                    backgroundColor: '#f7f5f5',
                    borderRadius: 15,
                  }}>
                  <Text style={{color: '#5cccee', fontSize: 20}}>
                    <MoneyIcon name="money-bill" size={23} />
                    Privacy Settings
                  </Text>
                </Button>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 20,
                }}>
                <Button
                  style={{
                    width: '95%',
                    backgroundColor: '#f7f5f5',
                    borderRadius: 15,
                  }}>
                  <Text style={{color: '#5cccee', fontSize: 20}}>
                    <CreditIcon name="credit" size={23} />
                    Vouchers and Credit
                  </Text>
                </Button>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 20,
                }}>
                <Button
                  style={{
                    width: '95%',
                    backgroundColor: '#f7f5f5',
                    borderRadius: 15,
                  }}>
                  <Text style={{color: '#5cccee', fontSize: 20}}>
                    <CreditIcon name="address" size={23} />
                    Saved Addresses
                  </Text>
                </Button>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 20,
                }}>
                <Button
                  style={{
                    width: '70%',
                    backgroundColor: 'red',
                    justifyContent: 'center',
                    borderRadius: 30,
                  }}
                  onPress={() => {
                    Actions.reset('signup');
                  }}>
                  <Text
                    style={{
                      color: '#fff',
                      fontSize: 20,
                      textAlign: 'center',
                    }}>
                    Sign out
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        )}
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 150,
    height: 150,
    backgroundColor: '#5cccee',
    position: 'absolute',
  },
  avatarPlaceholder: {
    width: 150,
    height: 150,
    borderRadius: 50,
    marginTop: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  uploadPic: {
    height: 200,
    width: 200,
    borderRadius: 30,
  },
});

const mapStateToProps = state => ({
  userID: state.login.userID || {} || state.registerPage.userID,
  imageUrl: state.registerPage.imageUrl || {},
});
const mapActionsCreators = {};
export default connect(
  mapStateToProps,
  mapActionsCreators,
)(Settings);
