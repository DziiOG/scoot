import React from 'react';
import { View, Text, Image } from 'react-native';
import { Container, Body, Button, Footer, Content, H1, H2, H3, Thumbnail } from 'native-base';
import HeaderForFreeRides from './Header';

const uri = "https://pbs.twimg.com/media/Dm69waWXcAEaCDY?format=jpg&name=small";
class FreeRides extends React.Component {
   

    render(){
        return(
            <Container>
                
                <Content>
                
                    <H1></H1> 
                    <H1 style={{color: '#4863A0'}}>  Want more Scoot </H1>
                    <H3></H3>
                    <H1 style={{color: '#4863A0'}}>  for less?</H1> 
                    <Text style={{color: '#4863A0'}}>    Get a free trip when you refer Scoot</Text>
                    <Text style={{color: '#4863A0'}}>    to a friend</Text>
                    <H1></H1>
                    <View style={{ justifyContent: 'center',
                    alignItems: 'center',}}>
                    <Thumbnail square large source={{uri: uri}} style={{width: '90%', height: 300 }} />
                    </View>
                    

                    </Content>
                <View>
                    <Image></Image>
                </View>
                <Footer style={{backgroundColor: 'transparent', position: 'absolute', bottom: 0, }}>
                    <Button style= {{ backgroundColor: '#4863A0', width: '50%', justifyContent: 'center'}}>
                        <Text style={{color: '#fff', textAlign: 'center'}}>INVITE FRIENDS</Text>
                    </Button>
                </Footer>
            </Container>
        );
    }
}

export default FreeRides;